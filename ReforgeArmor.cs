﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.IO;
using System;
using Terraria.GameContent.Events;
using Terraria.GameInput;
using Terraria.ID;
using Terraria.ModLoader.IO;
using Terraria.ModLoader;
using Terraria.UI;
using Terraria;

namespace ReforgeArmor {
	public class ReforgeArmor : Mod {
		public ReforgeArmor() {
			this.Properties = new ModProperties() {
				Autoload = true
			};
		}
	}

	public class RAGlobalItem : GlobalItem {
		public override bool CloneNewInstances { get { return true; } }
		public override bool InstancePerEntity { get { return true; } }

		public bool IsArmor(Item item) {
			return ((item.headSlot != -1 || item.bodySlot != -1 || item.legSlot != -1) && !item.vanity);
		}

		public override void UpdateInventory(Item item, Player player) {
			if (item.type == 0) { return; }
			HackItem(item);
			ReValue(item);
		}

		public override void UpdateEquip(Item item, Player player) {
			if (item.type == 0) { return; }
			HackItem(item);
			ReValue(item);
		}

		public override bool CanEquipAccessory(Item item, Player player, int slot) {
			if (IsArmor(item)) {
				return false;
			}
			return base.CanEquipAccessory(item, player, slot);
		}

		public override void PostReforge(Item item) { ReforgeArmor(item, -2); }
		public override void OnCraft(Item item, Recipe recipe) { ReforgeArmor(item, -2); }

		public void ReforgeArmor(Item item, int prefix) {
			if (IsArmor(item)) {
				HackItem(item);
				item.Prefix(prefix);
				ReValue(item);
			}
		}

		public void HackItem(Item item) {
			if (IsArmor(item) && !item.accessory) {
				item.accessory = true;
			}
		}

		public void ReValue(Item item) {
			if (item.value <= 1 && item.rare > 0 && (IsArmor(item) || item.accessory)) {
				item.value = (int)(item.rare * item.defense * 2500);
				item.Prefix(item.prefix);
			}
		}
	}
}
